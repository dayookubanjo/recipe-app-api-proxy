#!/bin/sh

set -e

# Get all the environment var values in original file and output to new file after >
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;' # All logs from nginx gets printed in our docker output